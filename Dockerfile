FROM drush/drush:8

RUN git config --global user.name "BitBucket Pipelines"
RUN git config --global user.email "noreply@bitbucket.org"

RUN mkdir -p ~/.ssh
RUN ssh-keyscan -T 10 -t rsa bitbucket.org >> ~/.ssh/known_hosts
RUN [ -s ~/.ssh/known_hosts ] || { echo "ssh-keyscan failed."; exit 1; }

ENTRYPOINT [ "/bin/sh", "-c", "/bin/bash" ]
