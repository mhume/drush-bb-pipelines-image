# Drush Docker Image for BitBucket Pipelines

This extends the [drush/drush](https://hub.docker.com/r/drush/drush/) image with some BitBucket Pipelines specific features.
